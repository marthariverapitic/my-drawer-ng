import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";

@Component({
  selector: 'ns-parte2',
  templateUrl: './parte2.component.html',
  styleUrls: ['./parte2.component.css'],
  moduleId: module.id,
})
export class parte2Component implements OnInit {

  constructor(private routerExtensions: RouterExtensions) { }

  ngOnInit() {
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onDrawerButtonClick(): void {    
    this.routerExtensions.navigate(["/parte1"], {
      transition: {
          name: "fade"
      }
  });
  }

}
