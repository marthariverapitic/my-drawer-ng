import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NewsServiceService } from '../news-service.service';
import { RouterExtensions } from 'nativescript-angular/router';
import { View, Color } from 'tns-core-modules/ui/page/page';
import * as Toast from 'nativescript-toast';
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../news-state.model";
import { Store, State } from '@ngrx/store';
import * as SocialShare from "nativescript-social-share";

@Component({
  selector: 'ns-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css'],
  moduleId: module.id,
})
export class ListaComponent implements OnInit {
  
  resultados: Array<string>;
  @ViewChild("layout", null) layout: ElementRef;

  constructor(private news: NewsServiceService, private routerExtensions: RouterExtensions, private store: Store<AppState>) {

  }

  ngOnInit() {
    this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
          const f = data;
          if (f != null) {
            const toast = Toast.makeText("Sugerimos leer " + f.titulo);
            toast.setDuration(5);
            toast.show();
          }
        });
  }

  onItemTap($event) {
    
    this.store.dispatch(new NuevaNoticiaAction(new Noticia("Noticia de prueba")));
  }

  onPull(e) {
    const pullRefresh = e.object;
    setTimeout(() => {
      // this.news.agregar("Noticia " + (this.news.buscar().length + 1));
      pullRefresh.refreshing = false;
    }, 1000);
  }
  onLongPress(s) {
    console.log(s);
    SocialShare.shareText(s, "Asunto: Compartido desde el curso!");
  }
  buscar(s: string) {
    console.log(s);
    this.news.buscar(s)
             .then((r: any) => {
               this.resultados = r;
             }, (e) => {
               const toast = Toast.makeText("Error en la búsqueda", "short");
               toast.show();
             });

    const layout = <View> this.layout.nativeElement;
    layout.animate({
      backgroundColor: new Color("blue"),
      duration: 300,
      delay: 150
    }).then(() => layout.animate({
      backgroundColor: new Color("green"),
      duration: 300,
      delay: 150
    }));
  }

  

}
