import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { filter } from "rxjs/operators";
import * as app from "tns-core-modules/application";
import { registerElement } from "nativescript-angular/element-registry";
import { Message } from "nativescript-plugin-firebase";
import * as Toast from "nativescript-toast";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);
const firebase = require("nativescript-plugin-firebase");

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {
    private _activatedUrl: string;
    private _sideDrawerTransition: DrawerTransitionBase;

    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject services.
    }

    ngOnInit(): void {
        this._activatedUrl = "/home";
        this._sideDrawerTransition = new SlideInOnTopTransition();

        this.router.events
        .pipe(filter((event: any) => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => this._activatedUrl = event.urlAfterRedirects);
        //invoca a firebase, manda un objeto de configuracion
        firebase.init({
            onMessageReceivedCallback: (message: Message) => {//callback de mensaje recibido
                console.log(`Titulo: ${message.title}`);
                console.log(`Cuerpo: ${message.body}`);
                console.log(`Data: ${JSON.stringify(message.data)}`);
                const toast = Toast.makeText("Notificacion: " + message.title);
                toast.setDuration(100);
                toast.show();
            },
            //cuando el cel nos asigna un token
            onPushTokenReceivedCallback: (token) => console.log("Firebase Push Token: " + token)
        }).then(() => console.log("firebase.init done"),//si se inicializo bien
                (error) => console.log("firebase.init error: " + error));//si tuvo error
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isComponentSelected(url: string): boolean {
        return this._activatedUrl === url;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
