import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { registerElement } from "nativescript-angular/element-registry";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
var gmaps = require("nativescript-google-maps-sdk");
@Component({
    selector: "Browse",
    moduleId: module.id,
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    @ViewChild("MapView", null) mapView: ElementRef;
    
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    //Map Events
    onMapReady(args) {
        console.log("Map Ready!!");
        var mapView= args.object;
        var marker = new gmaps.Marker();
        marker.position = gmaps.Position.positionFromLatLng(-34.6037,-58.3817);
        marker.title= "Mexico";
        marker.snippet = "hermosillo";
        marker.userData = { index:1};
        mapView.addMarker(marker);
    }
}
