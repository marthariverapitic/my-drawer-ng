import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { CustomRoutingModule } from "./custom-routing-module.module";
import { parte1Component } from "../parte1/parte1.component";
import { parte2Component } from "../parte2/parte2.component";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    CustomRoutingModule
  ],
  declarations: [
    parte1Component,
    parte2Component
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CustomModuleModule { }
