import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { parte1Component } from "../parte1/parte1.component";
import { parte2Component } from "../parte2/parte2.component";

const routes: Routes = [
    { path: "", redirectTo: "/parte1", pathMatch: "full" },
    { path: "parte1", component: parte1Component },
    { path: "parte2", component: parte2Component }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CustomRoutingModule { }
