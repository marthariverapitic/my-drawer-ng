import { Component, EventEmitter,  Input,Output, OnInit, Directive } from '@angular/core';


@Component({
    selector: "SearchForms",
    moduleId: module.id,
    template:
    `<TextField #texto="ngModel" [(ngModel)] ="textFieldValue" hint="Ingresar Texto..." required minlen="3"></TextField>
    <Label *ngIf="texto.hasError('required')" text="Llene los campos para la busqueda"></Label>
    <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="3+"></Label>
    <Button text="Buscar" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>
    `

})



export class SearchFormComponent implements OnInit {
    textFieldValue: string = "";
    //donde se use ,usa parentesis search
    @Output() search: EventEmitter<string> = new EventEmitter();//cuando detecte que se cumple la condicion, emite el evento
    @Input() inicial: string;

    ngOnInit(): void {
        this.textFieldValue = this.inicial;
    }
    
    onButtonTap(): void{
        console.log(this.textFieldValue);
        if(this.textFieldValue.length > 2) {
            this.search.emit(this.textFieldValue);
        }
    }
}

