import { Component, OnInit } from '@angular/core';
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from 'nativescript-toast';

@Component({
  selector: 'ns-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
  moduleId: module.id,
})
export class DetalleComponent implements OnInit {

  comments: Array<any> = [];

  constructor() {
    this.comments.push({Usuario: "Juan Perez", Fecha: "21/07/2019", Texto: "Excelente noticia."});
    this.comments.push({Usuario: "Róger Ordóñez", Fecha: "21/07/2019", Texto: "Muchas gracias por la noticia."});
    this.comments.push({Usuario: "Kenia Valverde", Fecha: "21/07/2019", Texto: "¡Qué interesante noticia!"});
    console.log(this.comments);
  }

  ngOnInit() {}

  onPull(e) {
    const pullRefresh = e.object;
    setTimeout(() => {
      this.comments.splice(0, 0, this.comments[Math.floor(Math.random() * (this.comments.length - 0)) + 0]);
      this.comments.splice(0, 0, this.comments[Math.floor(Math.random() * (this.comments.length - 0)) + 0]);      
      const  toast = Toast.makeText("Comentarios actualizados", "short");
      toast.show();
      pullRefresh.refreshing = false;
    }, 1000);
  }

  onItemTap(e) {
    dialogs.action("Control de comentarios", "Cancelar", ["Reportar como spam", "Denunciar comentario"])
           .then((result) => {
              console.log(result);
              if (result === "Reportar como spam") {
                dialogs.alert({
                    title: "Reportar como spam",
                    message: "Se ha reportado como spam el comentario indicado, muchas gracias.",
                    okButtonText: "Aceptar"
                }).then(() => console.log("Mensaje cerrado"));
              } else if (result === "Denunciar comentario") {
                dialogs.alert({
                    title: "Denunciar comentario",
                    message: "Se ha denunciado el comentario indicado, muchas gracias.",
                    okButtonText: "Aceptar"
                }).then(() => console.log("Mensaje cerrado"));
              }
           });
  }

}
