import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "users",
    moduleId: module.id,
    templateUrl: "./users.component.html"
})
export class usersComponent implements OnInit {
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
   
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    onButtonTap(): void{
        
        console.log(this.textFieldValue);
        if(this.textFieldValue.length > 4) {
            this.search.emit(this.textFieldValue);
            appSettings.setString("textFieldValue","hola");
            appSettings.setBoolean("estalogueado", true);
            const estalogueado = appSettings.getBoolean("estalogueado",false);

        
        }else{
            appSettings.clear();
        }
    }
}
