(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[3],{

/***/ "./app/search/search-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchRoutingModule", function() { return SearchRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-angular/router");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/search/search.component.ts");



var routes = [
    { path: "main", component: _search_component__WEBPACK_IMPORTED_MODULE_2__["SearchComponent"] }
];
var SearchRoutingModule = /** @class */ (function () {
    function SearchRoutingModule() {
    }
    SearchRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], SearchRoutingModule);
    return SearchRoutingModule;
}());



/***/ }),

/***/ "./app/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar class=\"action-bar\">\r\n    <!-- \r\n    Use the NavigationButton as a side-drawer button in Android\r\n    because ActionItems are shown on the right side of the ActionBar\r\n    -->\r\n    <NavigationButton ios:visibility=\"collapsed\" icon=\"res://menu\" (tap)=\"onDrawerButtonTap()\"></NavigationButton>\r\n    <!-- \r\n    Use the ActionItem for IOS with position set to left. Using the\r\n    NavigationButton as a side-drawer button in iOS is not possible,\r\n    because its function is to always navigate back in the application.\r\n    -->\r\n    <ActionItem icon=\"res://navigation/menu\" android:visibility=\"collapsed\" (tap)=\"onDrawerButtonTap()\"\r\n        ios.position=\"left\">\r\n    </ActionItem>\r\n    <Label class=\"action-bar-title\" text=\"Search\"></Label>\r\n</ActionBar>\r\n\r\n\r\n<!--\r\n<PullToRefresh (refresh)=\"onPull($event)\">\r\n        <ListView class=\"list-group\" [items]=\"this.resultados\">\r\n            <ng-template let-x =\"item\">\r\n                    <FlexboxLayout flexDirection=\"row\" class=\"list-group-item\"> \r\n                        <Image src=\"res://icon\" class=\"thumb img-circle\" ></Image> \r\n                            <Label [text]=\"x\" class=\"list-group-item-heading\" \r\n                            verticalAlignment=\"center\" style=\"width: 60%\"> \r\n                            </Label> \r\n                    </FlexboxLayout>\r\n            </ng-template>\r\n        </ListView>\r\n    </PullToRefresh> -->\r\n\r\n    <StackLayout class=\"page page-content\">\r\n        <Label class=\"page-icon fa\" text=\"&#xf002;\"></Label>\r\n        <ListView class=\"list-group\" [items]=\"this.noticias.buscar()\" (itemTap)=\"onItemTap($event)\" style=\"height: 1250px\" >\r\n            <ng-template let-x=\"item\">\r\n                <FlexboxLayaout flexDirection=\"row\" class=\"list-group-item\">\r\n                    <image src=\"res://icon\" class=\"thumb img-circle\"></image>\r\n                    <label [text]=\"x\" class=\"list-group-item-heading\" verticalAlignment=\"center\" style=\"width:60%\"></label>\r\n                </FlexboxLayaout>\r\n            </ng-template>\r\n        </ListView>\r\n      </StackLayout>\r\n    \r\n      <GridLayaout columns=\"auto,*,auto\" rows=\"auto,25\" verticalAlignment=\"top\" backgroundColor=\"lightgray\">\r\n            <Image src=\"~/icon.jpg\" rowSpan=\"2\" wigth=\"72\" height=\"72\" margin=\"3\" verticalAlignment=\"top\"></Image>\r\n            <Label text=\"Excelente hotel!\" textWrap=\"true\" col=\"1\" colSpan=\"2\" minHeight=\"50\" fontSize=\"14\"\r\n            horizontalAlignment=\"left\" verticalAlignment=\"bottom\" margin=\"3\">\r\n        </Label>\r\n        <Label text=\"Usuario:martha Rivera\" col=\"1\" row=\"1\" fontSize=\"14\" horizontalAlignment=\"left\" verticalAlignment=\"bottom\"\r\n        margin=\"3\">\r\n        </Label>\r\n        <Label text=\"Puntaje: 4.3\" col=\"2\" row=\"1\" color=\"#10C2B0\" fontSize=\"14\" verticalAlignment=\"bottom\" margin=\"3\"></Label>\r\n     </GridLayaout>\r\n    \r\n    "

/***/ }),

/***/ "./app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("tns-core-modules/application");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _domain_noticia_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/domain/noticia.service.ts");



var SearchComponent = /** @class */ (function () {
    function SearchComponent(noticias) {
        this.noticias = noticias;
        this.resultados = [];
        // Use the component constructor to inject providers.
    }
    SearchComponent.prototype.onPull = function (e) {
        var _this = this;
        console.log(e);
        var pullRefresh = e.object;
        setTimeout(function () {
            _this.resultados.push("xxxxxxx");
            pullRefresh.refreshing = false;
        }, 2000);
    };
    SearchComponent.prototype.ngOnInit = function () {
        // Init your component properties here.
        //evento de angular
        /*console.log("prueba");
        console.log({nombre: {nombre: {nombre:{nombre:"martha"}}}});
        console.dir({nombre: {nombre: {nombre:{nombre:"martha"}}}});//muestra un poco mas de info
        console.log([1,2,3]);
        console.dir([4,5,6]);*/
        this.noticias.agregar("hola");
        this.noticias.agregar("hola 2");
        this.noticias.agregar("hola 3");
    };
    SearchComponent.prototype.onDrawerButtonTap = function () {
        var sideDrawer = tns_core_modules_application__WEBPACK_IMPORTED_MODULE_1__["getRootView"]();
        sideDrawer.showDrawer();
    };
    SearchComponent.prototype.onItemTap = function (x) {
        console.dir(x);
    };
    SearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Search",
            template: __webpack_require__("./app/search/search.component.html") /*,
            providers:[NoticiasService]*/
        }),
        __metadata("design:paramtypes", [_domain_noticia_service__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./app/search/search.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchModule", function() { return SearchModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("@angular/core");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_angular_core__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-angular/common");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _search_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/search/search-routing.module.ts");
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/search/search.component.ts");




var SearchModule = /** @class */ (function () {
    function SearchModule() {
    }
    SearchModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                _search_routing_module__WEBPACK_IMPORTED_MODULE_2__["SearchRoutingModule"]
            ],
            declarations: [
                _search_component__WEBPACK_IMPORTED_MODULE_3__["SearchComponent"]
            ],
            //providers: [NoticiasService],
            schemas: [
                _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], SearchModule);
    return SearchModule;
}());

;
    if (true) {
        module.hot.accept();
        module.hot.dispose(() => {
            // Currently the context is needed only for application style modules.
            const moduleContext = {};
            global.hmrRefresh(moduleContext);
        });
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvc2VhcmNoL3NlYXJjaC1yb3V0aW5nLm1vZHVsZS50cyIsIndlYnBhY2s6Ly8vLi9hcHAvc2VhcmNoL3NlYXJjaC5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9hcHAvc2VhcmNoL3NlYXJjaC5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXlDO0FBRThCO0FBRWxCO0FBRXJELElBQU0sTUFBTSxHQUFXO0lBQ25CLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsaUVBQWUsRUFBRTtDQUMvQyxDQUFDO0FBTUY7SUFBQTtJQUFtQyxDQUFDO0lBQXZCLG1CQUFtQjtRQUovQiw4REFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0ZBQXdCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BELE9BQU8sRUFBRSxDQUFDLG9GQUF3QixDQUFDO1NBQ3RDLENBQUM7T0FDVyxtQkFBbUIsQ0FBSTtJQUFELDBCQUFDO0NBQUE7QUFBSjs7Ozs7Ozs7QUNkaEMsNmxEQUE2bEQsODdDOzs7Ozs7OztBQ0E3bEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBa0Q7QUFFRTtBQUNRO0FBUTVEO0lBWUkseUJBQW9CLFFBQXlCO1FBQXpCLGFBQVEsR0FBUixRQUFRLENBQWlCO1FBVjdDLGVBQVUsR0FBa0IsRUFBRSxDQUFDO1FBVzNCLHFEQUFxRDtJQUN6RCxDQUFDO0lBWEQsZ0NBQU0sR0FBTixVQUFPLENBQUM7UUFBUixpQkFPQztRQU5HLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDZixJQUFNLFdBQVcsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzdCLFVBQVUsQ0FBQztZQUNQLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2hDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25DLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNiLENBQUM7SUFNRCxrQ0FBUSxHQUFSO1FBQ0ksdUNBQXVDO1FBQ3ZDLG1CQUFtQjtRQUNuQjs7OzsrQkFJdUI7UUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELDJDQUFpQixHQUFqQjtRQUNJLElBQU0sVUFBVSxHQUFrQix3RUFBZSxFQUFFLENBQUM7UUFDcEQsVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFDRCxtQ0FBUyxHQUFULFVBQVUsQ0FBQztRQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbkIsQ0FBQztJQW5DUSxlQUFlO1FBTjNCLCtEQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsUUFBUTtZQUVsQixtRUFBc0M7eUNBQ1Q7U0FDaEMsQ0FBQzt5Q0FhZ0MsdUVBQWU7T0FacEMsZUFBZSxDQXFDM0I7SUFBRCxzQkFBQztDQUFBO0FBckMyQjs7Ozs7Ozs7O0FDWDVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkQ7QUFDWTtBQUVUO0FBQ1Q7QUFnQnJEO0lBQUE7SUFBNEIsQ0FBQztJQUFoQixZQUFZO1FBYnhCLDhEQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsb0ZBQXdCO2dCQUN4QiwwRUFBbUI7YUFDdEI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YsaUVBQWU7YUFDbEI7WUFDRCwrQkFBK0I7WUFDL0IsT0FBTyxFQUFFO2dCQUNMLDhEQUFnQjthQUNuQjtTQUNKLENBQUM7T0FDVyxZQUFZLENBQUk7SUFBRCxtQkFBQztDQUFBO0FBQUoiLCJmaWxlIjoiMy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVzIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBTZWFyY2hDb21wb25lbnQgfSBmcm9tIFwiLi9zZWFyY2guY29tcG9uZW50XCI7XHJcblxyXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuICAgIHsgcGF0aDogXCJtYWluXCIsIGNvbXBvbmVudDogU2VhcmNoQ29tcG9uZW50IH1cclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyldLFxyXG4gICAgZXhwb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlYXJjaFJvdXRpbmdNb2R1bGUgeyB9XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8QWN0aW9uQmFyIGNsYXNzPVxcXCJhY3Rpb24tYmFyXFxcIj5cXHJcXG4gICAgPCEtLSBcXHJcXG4gICAgVXNlIHRoZSBOYXZpZ2F0aW9uQnV0dG9uIGFzIGEgc2lkZS1kcmF3ZXIgYnV0dG9uIGluIEFuZHJvaWRcXHJcXG4gICAgYmVjYXVzZSBBY3Rpb25JdGVtcyBhcmUgc2hvd24gb24gdGhlIHJpZ2h0IHNpZGUgb2YgdGhlIEFjdGlvbkJhclxcclxcbiAgICAtLT5cXHJcXG4gICAgPE5hdmlnYXRpb25CdXR0b24gaW9zOnZpc2liaWxpdHk9XFxcImNvbGxhcHNlZFxcXCIgaWNvbj1cXFwicmVzOi8vbWVudVxcXCIgKHRhcCk9XFxcIm9uRHJhd2VyQnV0dG9uVGFwKClcXFwiPjwvTmF2aWdhdGlvbkJ1dHRvbj5cXHJcXG4gICAgPCEtLSBcXHJcXG4gICAgVXNlIHRoZSBBY3Rpb25JdGVtIGZvciBJT1Mgd2l0aCBwb3NpdGlvbiBzZXQgdG8gbGVmdC4gVXNpbmcgdGhlXFxyXFxuICAgIE5hdmlnYXRpb25CdXR0b24gYXMgYSBzaWRlLWRyYXdlciBidXR0b24gaW4gaU9TIGlzIG5vdCBwb3NzaWJsZSxcXHJcXG4gICAgYmVjYXVzZSBpdHMgZnVuY3Rpb24gaXMgdG8gYWx3YXlzIG5hdmlnYXRlIGJhY2sgaW4gdGhlIGFwcGxpY2F0aW9uLlxcclxcbiAgICAtLT5cXHJcXG4gICAgPEFjdGlvbkl0ZW0gaWNvbj1cXFwicmVzOi8vbmF2aWdhdGlvbi9tZW51XFxcIiBhbmRyb2lkOnZpc2liaWxpdHk9XFxcImNvbGxhcHNlZFxcXCIgKHRhcCk9XFxcIm9uRHJhd2VyQnV0dG9uVGFwKClcXFwiXFxyXFxuICAgICAgICBpb3MucG9zaXRpb249XFxcImxlZnRcXFwiPlxcclxcbiAgICA8L0FjdGlvbkl0ZW0+XFxyXFxuICAgIDxMYWJlbCBjbGFzcz1cXFwiYWN0aW9uLWJhci10aXRsZVxcXCIgdGV4dD1cXFwiU2VhcmNoXFxcIj48L0xhYmVsPlxcclxcbjwvQWN0aW9uQmFyPlxcclxcblxcclxcblxcclxcbjwhLS1cXHJcXG48UHVsbFRvUmVmcmVzaCAocmVmcmVzaCk9XFxcIm9uUHVsbCgkZXZlbnQpXFxcIj5cXHJcXG4gICAgICAgIDxMaXN0VmlldyBjbGFzcz1cXFwibGlzdC1ncm91cFxcXCIgW2l0ZW1zXT1cXFwidGhpcy5yZXN1bHRhZG9zXFxcIj5cXHJcXG4gICAgICAgICAgICA8bmctdGVtcGxhdGUgbGV0LXggPVxcXCJpdGVtXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxGbGV4Ym94TGF5b3V0IGZsZXhEaXJlY3Rpb249XFxcInJvd1xcXCIgY2xhc3M9XFxcImxpc3QtZ3JvdXAtaXRlbVxcXCI+IFxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcInJlczovL2ljb25cXFwiIGNsYXNzPVxcXCJ0aHVtYiBpbWctY2lyY2xlXFxcIiA+PC9JbWFnZT4gXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBbdGV4dF09XFxcInhcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwLWl0ZW0taGVhZGluZ1xcXCIgXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiIHN0eWxlPVxcXCJ3aWR0aDogNjAlXFxcIj4gXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+IFxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9GbGV4Ym94TGF5b3V0PlxcclxcbiAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XFxyXFxuICAgICAgICA8L0xpc3RWaWV3PlxcclxcbiAgICA8L1B1bGxUb1JlZnJlc2g+IC0tPlxcclxcblxcclxcbiAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcInBhZ2UgcGFnZS1jb250ZW50XFxcIj5cXHJcXG4gICAgICAgIDxMYWJlbCBjbGFzcz1cXFwicGFnZS1pY29uIGZhXFxcIiB0ZXh0PVxcXCImI3hmMDAyO1xcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgIDxMaXN0VmlldyBjbGFzcz1cXFwibGlzdC1ncm91cFxcXCIgW2l0ZW1zXT1cXFwidGhpcy5ub3RpY2lhcy5idXNjYXIoKVxcXCIgKGl0ZW1UYXApPVxcXCJvbkl0ZW1UYXAoJGV2ZW50KVxcXCIgc3R5bGU9XFxcImhlaWdodDogMTI1MHB4XFxcIiA+XFxyXFxuICAgICAgICAgICAgPG5nLXRlbXBsYXRlIGxldC14PVxcXCJpdGVtXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPEZsZXhib3hMYXlhb3V0IGZsZXhEaXJlY3Rpb249XFxcInJvd1xcXCIgY2xhc3M9XFxcImxpc3QtZ3JvdXAtaXRlbVxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8aW1hZ2Ugc3JjPVxcXCJyZXM6Ly9pY29uXFxcIiBjbGFzcz1cXFwidGh1bWIgaW1nLWNpcmNsZVxcXCI+PC9pbWFnZT5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBbdGV4dF09XFxcInhcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwLWl0ZW0taGVhZGluZ1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgc3R5bGU9XFxcIndpZHRoOjYwJVxcXCI+PC9sYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgPC9GbGV4Ym94TGF5YW91dD5cXHJcXG4gICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxcclxcbiAgICAgICAgPC9MaXN0Vmlldz5cXHJcXG4gICAgICA8L1N0YWNrTGF5b3V0PlxcclxcbiAgICBcXHJcXG4gICAgICA8R3JpZExheWFvdXQgY29sdW1ucz1cXFwiYXV0bywqLGF1dG9cXFwiIHJvd3M9XFxcImF1dG8sMjVcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJ0b3BcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwibGlnaHRncmF5XFxcIj5cXHJcXG4gICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJ+L2ljb24uanBnXFxcIiByb3dTcGFuPVxcXCIyXFxcIiB3aWd0aD1cXFwiNzJcXFwiIGhlaWdodD1cXFwiNzJcXFwiIG1hcmdpbj1cXFwiM1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcInRvcFxcXCI+PC9JbWFnZT5cXHJcXG4gICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiRXhjZWxlbnRlIGhvdGVsIVxcXCIgdGV4dFdyYXA9XFxcInRydWVcXFwiIGNvbD1cXFwiMVxcXCIgY29sU3Bhbj1cXFwiMlxcXCIgbWluSGVpZ2h0PVxcXCI1MFxcXCIgZm9udFNpemU9XFxcIjE0XFxcIlxcclxcbiAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImxlZnRcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJib3R0b21cXFwiIG1hcmdpbj1cXFwiM1xcXCI+XFxyXFxuICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgPExhYmVsIHRleHQ9XFxcIlVzdWFyaW86bWFydGhhIFJpdmVyYVxcXCIgY29sPVxcXCIxXFxcIiByb3c9XFxcIjFcXFwiIGZvbnRTaXplPVxcXCIxNFxcXCIgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwibGVmdFxcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImJvdHRvbVxcXCJcXHJcXG4gICAgICAgIG1hcmdpbj1cXFwiM1xcXCI+XFxyXFxuICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgPExhYmVsIHRleHQ9XFxcIlB1bnRhamU6IDQuM1xcXCIgY29sPVxcXCIyXFxcIiByb3c9XFxcIjFcXFwiIGNvbG9yPVxcXCIjMTBDMkIwXFxcIiBmb250U2l6ZT1cXFwiMTRcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJib3R0b21cXFwiIG1hcmdpbj1cXFwiM1xcXCI+PC9MYWJlbD5cXHJcXG4gICAgIDwvR3JpZExheWFvdXQ+XFxyXFxuICAgIFxcclxcbiAgICBcIiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlclwiO1xyXG5pbXBvcnQgKiBhcyBhcHAgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIjtcclxuaW1wb3J0IHsgTm90aWNpYXNTZXJ2aWNlIH0gZnJvbSBcIi4uL2RvbWFpbi9ub3RpY2lhLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwiU2VhcmNoXCIsLy9ub21icmUgZGVsIFRBR1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcIi4vc2VhcmNoLmNvbXBvbmVudC5odG1sXCIvKixcclxuICAgIHByb3ZpZGVyczpbTm90aWNpYXNTZXJ2aWNlXSovXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHJlc3VsdGFkb3M6IEFycmF5PHN0cmluZz4gPSBbXTsgXHJcbiAgICBvblB1bGwoZSkgeyBcclxuICAgICAgICBjb25zb2xlLmxvZyhlKTsgXHJcbiAgICAgICAgY29uc3QgcHVsbFJlZnJlc2ggPSBlLm9iamVjdDsgXHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7IFxyXG4gICAgICAgICAgICB0aGlzLnJlc3VsdGFkb3MucHVzaChcInh4eHh4eHhcIik7IFxyXG4gICAgICAgICAgICBwdWxsUmVmcmVzaC5yZWZyZXNoaW5nID0gZmFsc2U7IFxyXG4gICAgICAgIH0sIDIwMDApOyBcclxuICAgIH0gXHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBub3RpY2lhczogTm90aWNpYXNTZXJ2aWNlKSB7XHJcbiAgICAgICAgLy8gVXNlIHRoZSBjb21wb25lbnQgY29uc3RydWN0b3IgdG8gaW5qZWN0IHByb3ZpZGVycy5cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICAvLyBJbml0IHlvdXIgY29tcG9uZW50IHByb3BlcnRpZXMgaGVyZS5cclxuICAgICAgICAvL2V2ZW50byBkZSBhbmd1bGFyXHJcbiAgICAgICAgLypjb25zb2xlLmxvZyhcInBydWViYVwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyh7bm9tYnJlOiB7bm9tYnJlOiB7bm9tYnJlOntub21icmU6XCJtYXJ0aGFcIn19fX0pO1xyXG4gICAgICAgIGNvbnNvbGUuZGlyKHtub21icmU6IHtub21icmU6IHtub21icmU6e25vbWJyZTpcIm1hcnRoYVwifX19fSk7Ly9tdWVzdHJhIHVuIHBvY28gbWFzIGRlIGluZm9cclxuICAgICAgICBjb25zb2xlLmxvZyhbMSwyLDNdKTtcclxuICAgICAgICBjb25zb2xlLmRpcihbNCw1LDZdKTsqL1xyXG4gICAgICAgIHRoaXMubm90aWNpYXMuYWdyZWdhcihcImhvbGFcIik7XHJcbiAgICAgICAgdGhpcy5ub3RpY2lhcy5hZ3JlZ2FyKFwiaG9sYSAyXCIpO1xyXG4gICAgICAgIHRoaXMubm90aWNpYXMuYWdyZWdhcihcImhvbGEgM1wiKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRyYXdlckJ1dHRvblRhcCgpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBzaWRlRHJhd2VyID0gPFJhZFNpZGVEcmF3ZXI+YXBwLmdldFJvb3RWaWV3KCk7XHJcbiAgICAgICAgc2lkZURyYXdlci5zaG93RHJhd2VyKCk7XHJcbiAgICB9XHJcbiAgICBvbkl0ZW1UYXAoeCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnNvbGUuZGlyKHgpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcclxuXHJcbmltcG9ydCB7IFNlYXJjaFJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9zZWFyY2gtcm91dGluZy5tb2R1bGVcIjtcclxuaW1wb3J0IHsgU2VhcmNoQ29tcG9uZW50IH0gZnJvbSBcIi4vc2VhcmNoLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBOb3RpY2lhc1NlcnZpY2UgfSBmcm9tIFwiLi4vZG9tYWluL25vdGljaWEuc2VydmljZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgU2VhcmNoUm91dGluZ01vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIFNlYXJjaENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIC8vcHJvdmlkZXJzOiBbTm90aWNpYXNTZXJ2aWNlXSxcclxuICAgIHNjaGVtYXM6IFtcclxuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hNb2R1bGUgeyB9XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=